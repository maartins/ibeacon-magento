<?php
class Mage_XmlConnect_Block_Customer_Reward extends Mage_Downloadable_Block_Customer_Products_List
{
    /**
     * Render downloadable products list xml
     *
     * @return string
     */
    protected function _toHtml()
    {
        

            /** @var $xmlModel Mage_XmlConnect_Model_Simplexml_Element */
            $xmlModel = Mage::getModel('xmlconnect/simplexml_element', '<downloads></downloads>');
            
                $itemXmlObj = $xmlModel->addCustomChild('item');
        

        return $xmlModel->asNiceXml();
    }

    /**
     * Return url to download link
     *
     * @param Mage_Downloadable_Model_Link_Purchased_Item $item
     * @return string
     */
    public function getDownloadUrl($item)
    {
        return $this->getUrl('downloadable/download/link', array('id' => $item->getLinkHash(), '_secure' => true));
    }
}
