<?php
/**
 * Created by PhpStorm.
 * User: martinssaukums
 * Date: 21/05/16
 * Time: 17:45
 */
class Saukums_Beacons_Block_Adminhtml_Beacons_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct()
    {
        parent::__construct();
        $this->setId('grid_id');
        // $this->setDefaultSort('COLUMN_ID');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('saukums_beacons/beacons')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

       $this->addColumn('title',
           array(
               'header'=> $this->__('Title'),
               'width' => '50px',
               'index' => 'title'
           )
       );

        $this->addColumn('address',
            array(
                'header'=> $this->__('Address'),
                'width' => '50px',
                'index' => 'address'
            )
        );

        $this->addColumn('beacon_uuid',
            array(
                'header'=> $this->__('UUID'),
                'width' => '50px',
                'index' => 'beacon_uuid'
            )
        );

        $this->addColumn('beacon_major',
            array(
                'header'=> $this->__('Major'),
                'width' => '50px',
                'index' => 'beacon_major'
            )
        );

        $this->addColumn('beacon_minor',
            array(
                'header'=> $this->__('Minor'),
                'width' => '50px',
                'index' => 'beacon_minor'
            )
        );

        $this->addColumn('x',
            array(
                'header'=> $this->__('X coordinate'),
                'width' => '50px',
                'index' => 'x'
            )
        );

        $this->addColumn('y',
            array(
                'header'=> $this->__('Y coordinate'),
                'width' => '50px',
                'index' => 'y'
            )
        );

        $this->addExportType('*/*/exportCsv', $this->__('CSV'));
        
        $this->addExportType('*/*/exportExcel', $this->__('Excel XML'));
        
        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
       return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

        protected function _prepareMassaction()
    {
        $modelPk = Mage::getModel('saukums_beacons/beacons')->getResource()->getIdFieldName();
        $this->setMassactionIdField($modelPk);
        $this->getMassactionBlock()->setFormFieldName('ids');
        // $this->getMassactionBlock()->setUseSelectAll(false);
        $this->getMassactionBlock()->addItem('delete', array(
             'label'=> $this->__('Delete'),
             'url'  => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }
    }
