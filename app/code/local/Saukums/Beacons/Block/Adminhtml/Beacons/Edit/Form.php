<?php
/**
 * Created by PhpStorm.
 * User: martinssaukums
 * Date: 21/05/16
 * Time: 17:45
 */
class Saukums_Beacons_Block_Adminhtml_Beacons_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _getModel(){
        return Mage::registry('current_model');
    }

    protected function _getHelper(){
        return Mage::helper('saukums_beacons');
    }

    protected function _getModelTitle(){
        return 'Beacons';
    }

    protected function _prepareForm()
    {
        $model  = $this->_getModel();
        $modelTitle = $this->_getModelTitle();
        $form   = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/save'),
            'method'    => 'post'
        ));

        $fieldset   = $form->addFieldset('base_fieldset', array(
            'legend'    => $this->_getHelper()->__("$modelTitle Information"),
            'class'     => 'fieldset-wide',
        ));

        if ($model && $model->getId()) {
            $modelPk = $model->getResource()->getIdFieldName();
            $fieldset->addField($modelPk, 'hidden', array(
                'name' => $modelPk,
            ));
        }

        $fieldset->addField('title', 'text', array(
            'label'     => Mage::helper('saukums_beacons')->__('Title'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'title'
        ));
        $fieldset->addField('description', 'textarea', array(
            'label'     => Mage::helper('saukums_beacons')->__('Description'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'description'
        ));
        $fieldset->addField('address', 'text', array(
            'label'     => Mage::helper('saukums_beacons')->__('Address'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'address'
        ));
        $fieldset->addField('beacon_uuid', 'text', array(
            'label'     => Mage::helper('saukums_beacons')->__('UUID'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'beacon_uuid'
        ));
        $fieldset->addField('beacon_major', 'text', array(
            'label'     => Mage::helper('saukums_beacons')->__('Major'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'beacon_major'
        ));
        $fieldset->addField('beacon_minor', 'text', array(
            'label'     => Mage::helper('saukums_beacons')->__('Minor'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'beacon_minor'
        ));
        $fieldset->addField('x', 'text', array(
            'label'     => Mage::helper('saukums_beacons')->__('X coordinate'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'x'
        ));
        $fieldset->addField('y', 'text', array(
            'label'     => Mage::helper('saukums_beacons')->__('Y coordinate'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'y'
        ));

        $fieldset->addField('is_notiication', 'checkbox', array(
            'label'     => Mage::helper('saukums_beacons')->__('Enable notification for this iBeacon'),
            'onclick'   => 'this.value = this.checked ? 1 : 0;',
            'name'      => 'is_notiication',
        ));

        $fieldset->addField('state', 'select', array(
            'label' => Mage::helper('saukums_beacons')->__('Select State for action'),
            'title' => Mage::helper('saukums_beacons')->__('Select State for action'),
            'name' => 'state',
            'required' => true,
            'options' => array(
                '.Inside' => 'Inside',
                '.Outside' => 'Outside',
                '.Unknown' => 'Unknown',
            ),
        ));



        if($model){
            $form->setValues($model->getData());
        }
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}
