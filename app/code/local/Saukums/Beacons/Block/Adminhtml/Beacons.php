<?php
/**
 * Created by PhpStorm.
 * User: martinssaukums
 * Date: 21/05/16
 * Time: 17:45
 */
class Saukums_Beacons_Block_Adminhtml_Beacons extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct()
    {
        $this->_blockGroup      = 'saukums_beacons';
        $this->_controller      = 'adminhtml_beacons';
        // $this->_headerText      = $this->__('Grid Header Text');
        // $this->_addButtonLabel  = $this->__('Add Button Label');
        parent::__construct();
            }

    public function getCreateUrl()
    {
        return $this->getUrl('*/*/new');
    }

}

