<?php

$installer = $this;
$connection = $installer->getConnection();

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('saukums_beacons/beacons'),
        'x',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'default' => null,
            'comment' => 'x coordinate'
        )
    );
$installer->getConnection()
    ->addColumn($installer->getTable('saukums_beacons/beacons'),
        'y',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'default' => null,
            'comment' => 'y coordinate'
        )
    );

$installer->endSetup();