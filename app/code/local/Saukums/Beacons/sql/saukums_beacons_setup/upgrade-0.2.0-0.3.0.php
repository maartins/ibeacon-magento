<?php

$installer = $this;
$connection = $installer->getConnection();

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('saukums_beacons/beacons'),
        'is_notiication',
        Varien_Db_Ddl_Table::TYPE_BOOLEAN,
        null,
        array(
            'nullable' => false,
            'default' => false,
        )
    );
$installer->getConnection()
    ->addColumn($installer->getTable('saukums_beacons/beacons'),
        'state',
        Varien_Db_Ddl_Table::TYPE_TEXT,
        null,
        array(
            'length' => 255,
        )
    );

$installer->endSetup();