<?php
/**
 * Created by PhpStorm.
 * User: martinssaukums
 * Date: 21/05/16
 * Time: 17:39
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();
$installer->run("
DROP TABLE IF EXISTS {$this->getTable('saukums_beacons/beacons')};
CREATE TABLE {$this->getTable('saukums_beacons/beacons')} (
  `beacons_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `status` char(1) NOT NULL DEFAULT '0',
  `address` char(255) NOT NULL DEFAULT '0',
  `beacon_uuid` char(255) NOT NULL DEFAULT '0',
  `beacon_major` char(255) NOT NULL DEFAULT '0',
  `beacon_minor` char(255) NOT NULL DEFAULT '0',
  `created_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`beacons_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
");

$installer->endSetup();