<?php
/**
 * Created by PhpStorm.
 * User: martinssaukums
 * Date: 21/05/16
 * Time: 17:40
 */ 
class Saukums_Beacons_Model_Resource_Beacons_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('saukums_beacons/beacons');
    }

}