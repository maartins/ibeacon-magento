<?php
/**
 * Created by PhpStorm.
 * User: martinssaukums
 * Date: 21/05/16
 * Time: 17:40
 */

class Saukums_Beacons_Adminhtml_BeaconsController extends Mage_Adminhtml_Controller_Action {

    public function indexAction()
    {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('saukums_beacons/adminhtml_beacons'));
        $this->renderLayout();
    }

    public function exportCsvAction()
    {
        $fileName = 'Beacons_export.csv';
        $content = $this->getLayout()->createBlock('saukums_beacons/adminhtml_beacons_grid')->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportExcelAction()
    {
        $fileName = 'Beacons_export.xml';
        $content = $this->getLayout()->createBlock('saukums_beacons/adminhtml_beacons_grid')->getExcel();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('ids');
        if (!is_array($ids)) {
            $this->_getSession()->addError($this->__('Please select Beacons(s).'));
        } else {
            try {
                foreach ($ids as $id) {
                    $model = Mage::getSingleton('saukums_beacons/beacons')->load($id);
                    $model->delete();
                }

                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) have been deleted.', count($ids))
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('saukums_beacons')->__('An error occurred while mass deleting items. Please review log and try again.')
                );
                Mage::logException($e);

                return;
            }
        }
        $this->_redirect('*/*/index');
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('saukums_beacons/beacons');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->_getSession()->addError(
                    Mage::helper('saukums_beacons')->__('This Beacons no longer exists.')
                );
                $this->_redirect('*/*/');

                return;
            }
        }

        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('current_model', $model);

        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('saukums_beacons/adminhtml_beacons_edit'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        $redirectBack = $this->getRequest()->getParam('back', false);
        if ($data = $this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('id');
            $model = Mage::getModel('saukums_beacons/beacons');
            if ($id) {
                $model->load($id);
                if (!$model->getId()) {
                    $this->_getSession()->addError(
                        Mage::helper('saukums_beacons')->__('This Beacons no longer exists.')
                    );
                    $this->_redirect('*/*/index');

                    return;
                }
            }

            // save model
            try {
                $model->addData($data);
                $this->_getSession()->setFormData($data);
                $model->save();
                $this->_getSession()->setFormData(false);
                $this->_getSession()->addSuccess(
                    Mage::helper('saukums_beacons')->__('The Beacons has been saved.')
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $redirectBack = true;
            } catch (Exception $e) {
                $this->_getSession()->addError(Mage::helper('saukums_beacons')->__('Unable to save the Beacons.'));
                $redirectBack = true;
                Mage::logException($e);
            }

            if ($redirectBack) {
                $this->_redirect('*/*/edit', array('id' => $model->getId()));

                return;
            }
        }
        $this->_redirect('*/*/index');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                // init model and delete
                $model = Mage::getModel('saukums_beacons/beacons');
                $model->load($id);
                if (!$model->getId()) {
                    Mage::throwException(Mage::helper('saukums_beacons')->__('Unable to find a Beacons to delete.'));
                }
                $model->delete();
                // display success message
                $this->_getSession()->addSuccess(
                    Mage::helper('saukums_beacons')->__('The Beacons has been deleted.')
                );
                // go to grid
                $this->_redirect('*/*/index');

                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('saukums_beacons')->__('An error occurred while deleting Beacons data. Please review log and try again.')
                );
                Mage::logException($e);
            }
            // redirect to edit form
            $this->_redirect('*/*/edit', array('id' => $id));

            return;
        }
// display error message
        $this->_getSession()->addError(
            Mage::helper('saukums_beacons')->__('Unable to find a Beacons to delete.')
        );
// go to grid
        $this->_redirect('*/*/index');
    }
}